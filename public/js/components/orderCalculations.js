const calculations = (function () {
  const $orderTotalPriceInput = $("#order-total"),
    $orderDiscountInput = $("#order-discount"),
    $orderNetPriceInput = $("#order-after-discount"),
    $qtyDetailsInputs = $(".qty-details-input"),
    $productDetailsInputs = $(".product-details-input");

  /**
   * Event for When the user change the discount value
   */
  $orderDiscountInput.keyup(function () {
    const totalPrice = parseFloat($orderTotalPriceInput.val().trim() || 0),
      newDiscount = parseFloat($(this).val().trim() || 0);

    if (newDiscount > 100) {
      $orderDiscountInput.val(100);
      $orderNetPriceInput.val(0);
      return;
    }

    $orderNetPriceInput.val(calculateNetPrice(totalPrice, newDiscount));
  });

  /**
   * Event for When the user change the quantity of any product in the details
   */
  $qtyDetailsInputs.keyup(detailsChangeProcess);

  /**
   * Event for When the user change the product of any row in the details
   */
  $productDetailsInputs.change(detailsChangeProcess);

  /**
   * Calculate the net total price of the order (The price after discount)
   *
   * @param {Number|String} totalPrice Order Total Price
   * @param {Number|String} discountPercentage Discount Percentage
   *
   * @returns {Number} The total net price
   */
  function calculateNetPrice(totalPrice, discountPercentage) {
    totalPrice = parseFloat(totalPrice || 0);
    discountPercentage = parseFloat(discountPercentage || 0);

    return totalPrice - (totalPrice / 100) * discountPercentage;
  }

  /**
   * Calculate the total quantity
   *
   * @returns {Number} The total price
   */
  function calculateTotalPrice() {
    let totalPrice = 0;
    $qtyDetailsInputs.each(function () {
      const $selectedOption = $(this)
          .parent()
          .parent()
          .find(".product-details-input")
          .find(":selected"),
        productPrice = parseFloat(
          $selectedOption.attr("product-price").trim() || 0
        );
      totalPrice += productPrice * parseInt($(this).val().trim() || 0);
    });
    return totalPrice;
  }

  /**
   * Responsible for the process of details inputs changing like changing the quantity or the product
   *
   * @returns {void}
   */
  function detailsChangeProcess() {
    const newTotalPrice = calculateTotalPrice();
    $orderTotalPriceInput.val(newTotalPrice);
    $orderNetPriceInput.val(
      calculateNetPrice(
        newTotalPrice,
        parseFloat($orderDiscountInput.val().trim() || 0)
      )
    );
  }
})();
