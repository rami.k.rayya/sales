/**
 * Ask the user to confirm a process
 *
 * @returns {boolean} - True if the user confirmed False otherwise
 */
function confirmBtn() {
  const defaultMessage = "Are you sure ?",
    btnConfirmMessage = $(this).attr("confirm-message");
  return confirm(btnConfirmMessage || defaultMessage);
}

$(document).on(
  {
    click: confirmBtn,
  },
  ".confirm-btn"
);
