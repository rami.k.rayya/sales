/**
 * When the user select a person type we should :
 * Show user fields containers if the selected option data-show-user-fields attribute is true
 * Hide user fields containers if the selected option data-show-user-fields attribute is false
 */
const personTypeForm = (function () {
  /** @variables & @constants */
  const $userFileds = $(".user-field-container"); // Get all user fields containers

  /** @events */
  $("#person-type").on("change", toggleUserFieldsOnChange);

  /** @functions */

  /**
   *
   * Toggle the user fields containers (elements which have .user-field-container class) depend on the person type selectbox
   *
   * @param {object} $selectBox The select box as jquery DOM object
   *
   * @returns {void}
   */
  function toggleUserFieldsOnChange() {
    const $selectedOption = $(this).find("option:selected"); // Get the selected option
    const showUserFields =
      $selectedOption.attr("data-show-user-fields") == "true"; // Check wether the value of the data-show-user-fields attribute is true

    if (showUserFields) {
      // Show the fields by removing d-none class and replace it with d-block
      // Remove readonly user fields
      // Make user fields inputs required
      // Make person region select box optional
      $userFileds.removeClass("d-none").addClass("d-block");
      $userFileds.find("input").attr("readonly", false);
      $userFileds.find("input").attr("required", "required");
      $("#person-region").prop("required", false);
    } else {
      // Hide the fields by removing d-block and replace it with d-none
      // Make user fields inputs readonly
      // Reset user fields inputs values
      // Make user fields inputs not required
      // Make person region select box required
      $userFileds.removeClass("d-block").addClass("d-none");
      $userFileds.find("input").attr("readonly", true);
      $userFileds.find("input").val("");
      $userFileds.find("input").removeAttr("required");
      $("#person-region").prop("required", true);
    }
  }
})();

const checkPassword = (function () {
  /** @variables & @constants */

  const $passwordField = $("#user-password"),
    $passwordCheckField = $("#user-password-check"),
    $form = $("#person-create-form");

  /** @events */

  $form.submit(function () {
    return validateInputs();
  });

  $passwordField.add($passwordCheckField).keyup(validateInputs);

  /** @functions */

  /**
   *
   * Check if the passwords match
   * if so show message and add is-valid class to the passwords inputs
   * otherwise show error message and remove is-valid class and replace it with is-invalid
   *
   * @returns {Boolean} True if the two inputs values (The passwords) are equal
   */
  function validateInputs() {
    const firstValue = $passwordField.val().trim(),
      secondValue = $passwordCheckField.val().trim();

    if (firstValue != secondValue) {
      // The two password are not equal, so add is-invalid to the two inputs and show a messate on the second input (password check input)
      $passwordField.addClass("is-invalid").removeClass("is-valid");
      $passwordCheckField.addClass("is-invalid").removeClass("is-valid");
      return false;
    } else if (firstValue != "" && secondValue != "") {
      $passwordField.addClass("is-valid").removeClass("is-invalid");
      $passwordCheckField.addClass("is-valid").removeClass("is-invalid");
      return true;
    }
  }
})();
