<?php
define("HOST", "localhost");
define("DB_NAME", "new_sales");
define("USERNAME", "root");
define("DB_PASSWORD", "thisisthedatabasepassword");

try {
  $connection = new PDO("mysql:host=" . HOST . ";dbname=" . DB_NAME . ";charset=utf8mb4", USERNAME, DB_PASSWORD, [
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
      PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
      PDO::ATTR_EMULATE_PREPARES => false,
    ]
  );
} catch (Exception $ex) {
  die($ex->getMessage());
}
?>