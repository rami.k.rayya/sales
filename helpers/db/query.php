<?php

/**
 * 
 * Generate Select columns [] from table Query statement
 * 
 * @param array $columns - The name of the columns you want to get from the table
 * @param string $table - The table name
 * 
 * @return string
 */
function selectQueryStr($columns = [], $table) {
  // Check if the columns array length is zero then we select all the columns
  if (count($columns) == 0) $columnsStr = "*";
  else $columnsStr = implode(',', $columns); // Convert the array into string sperated with ','

  $querySelectPart = "SELECT " . $columnsStr . " FROM " . $table . " ";

  return $querySelectPart;
}

/**
 * 
 * Generate String contains Where Query statement with the provided conditions
 * 
 * @param array $whereStatements - Where Statements content
 * 
 * @return string
 */
function whereQueryStr($whereStatements = []) {
  $queryWherePart = "";
  foreach ($whereStatements as $index => $whereStatement) { // Loop through all the where statements to append them to the query string
    if ($index == 0) // The first where we use only WHERE Clause
      $queryWherePart .= "WHERE " . $whereStatement . " ";
    else // It is not the first so We need to add AND Clause
      $queryWherePart .= "AND " . $whereStatement . " ";
  }
  return $queryWherePart;
}

/**
 * 
 * Get one record data from the database
 * 
 * @param array $columns - The name of the columns you want to get from the table
 * @param string $table - The table name
 * @param array $whereStatements - Array of strings each string is an where statement string
 * 
 * @return array|false Array contains the columns and the value, But false if the record isn't exist
 */
function selectOne($columns = [], $table, $whereStatements = []) {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided
  global $connection;
  // Generate query string from the parameters
  $query = selectQueryStr($columns, $table) . whereQueryStr($whereStatements);

  try {
    $getOne = $connection->prepare($query);
    $getOne->execute();
    $fetchRespone = $getOne->fetch();
    return $fetchRespone; // Array or false value
  }
  catch (Exception $e) {
    return false;
  }
}

/**
 * 
 * Get all the records the match the query parameters
 * 
 * @param array $columns - The name of the columns you want to get from the table
 * @param string $table - The table name
 * @param array $whereStatements - Array of strings each string is an where statement string
 * 
 * @return array|false Array contains arrays each element is an array contains the record data, But false if the record isn't exist
 */
function selectAll($columns = [], $table, $whereStatements = []) {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  // Generate query string from the parameters
  $query = selectQueryStr($columns, $table) . whereQueryStr($whereStatements);

  try {
    $getAll = $connection->prepare($query);
    $getAll->execute();
    $fetchRespone = $getAll->fetchAll();
    return $fetchRespone; // Array or false value
  }
  catch (Exception $e) {
    return false;
  }
}

/**
 * 
 * Generate insert query string depend on the table and columns and their values
 * 
 * @param string $table - The Name of the table
 * @param array $columnsAndValues - The columns wanted to be added and their value
 * 
 * @return string The generated Query
 * 
 */
function insertQueryStr($table, $columnsAndValues) {
  $insertQuery = "INSERT INTO " . $table . " ";

  // Generate the complement of the insert query
  // The whole query should be something like this : INSERT INTO table_name (column1, column2) VALUES (column1Value, column2Value)
  // $columnsNamesOrder should be (column1, column2) 
  // $columnsValuesOrder should be (column1Value, column2Value)
  $columnsNamesOrder = "(";
  $columnsValuesOrder = "(";

  $counter = 0;
  foreach ($columnsAndValues as $column => $value) {
    if ($counter++ != 0) {
      $columnsNamesOrder .= ", "; // Add ', ' before all elements but the first one
      $columnsValuesOrder .= ", "; // Add ', ' before all elements but the first one
    }

    $columnsNamesOrder .= $column;
    $columnsValuesOrder .= "'" . $value . "'";
  }

  $columnsNamesOrder .= ")";
  $columnsValuesOrder .= ")";

  $insertQuery .= $columnsNamesOrder . " VALUES " . $columnsValuesOrder;

  return $insertQuery;
}

/**
 * Insert a record into the database
 * 
 * @param $table - The name of the table
 * @param array $columnsAndValues - The columns wanted to be added and their value
 * @param boolean $getLastInsertedId - 
 * 
 * @return bool|int True -> success | False -> failed | int [The id of the inserted row] (is $getLastInsertedId is true) 
 */
function insert($table, $columnsAndValues, $getLastInsertedId = false) {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  $queryStr = insertQueryStr($table, $columnsAndValues);

  try {
    $query = $connection->prepare($queryStr);
    $status = $query->execute();
    if (!$getLastInsertedId) return $status;
    return $connection->lastInsertId();
  }
  catch (Exception $e) {
    if ($getLastInsertedId) return 0;
    return false;
  }
}

/**
 * 
 * Generate Update table set columns and value (Update Query without where) query as a string
 * 
 * @param string $table - The name of the table
 * @param array $columnsAndValues - The columns wanted to be updated and the value (As key => value, key is the column name and value is its value)
 * 
 * @return string The Query string
 */
function updateQueryStr($table, $columnsAndValues) {
  $updateQuery = "UPDATE " . $table . " SET ";

  // Loop through the columns and values array, its keys are the columns name and the values are the new values of the columns
  $counter = 0;
  foreach($columnsAndValues as $column => $value) {
    if ($counter++ != 0) $updateQuery .= ", "; // Add ',' before all but the first one
    $updateQuery .= $column . " = '" . $value . "'";
  }

  $updateQuery .= " ";

  return $updateQuery;
}

/**
 * 
 * Update record/s in a specific table in the database
 * 
 * @param string $table - The Name of the table
 * @param array $whereStatements - Array of strings each string is an where statement string
 * @param array $columnsAndValues - The columns wanted to be updated and the value (As key => value, key is the column name and value is its value)
 * 
 * @return int The number of the updated records
 * 
 */
function update($table, $whereStatements, $columnsAndValues) {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  // Generate Update Query string
  $query = updateQueryStr($table, $columnsAndValues) . whereQueryStr($whereStatements);

  try {
    $statement = $connection->prepare($query);
    $statement->execute();
    return $statement->rowCount();
  }
  catch (Exception $e) {
    return 0;
  }
}

/**
 * 
 * Delete Record/s From a specific table in the database 
 * 
 * @param array $columns - The name of the columns you want to get from the table
 * @param array $whereStatements - Array of strings each string is an where statement string
 * 
 * @return int 0 Denotes that there is no records were deleted otherwise the number of the deleted records
 */
function delete($table, $whereStatements) {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  $query = "DELETE FROM " . $table . " " . whereQueryStr($whereStatements);
  
  try {
    $deleteStatement = $connection->prepare($query);
    $deleteStatement->execute();
    return $deleteStatement->rowCount();
  }
  catch (Exception $e) {
    return 0;
  }
}

/**
 * 
 * Check if a column value is unique in the table
 * 
 * @param string $column
 * @
 */
function isUnique($column, $table, $value) {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  $queryStr = selectQueryStr([$column], $table) . whereQueryStr([
    $column . " = '" . $value . "'"
  ]);

  try {
    $stmt = $connection->prepare($queryStr);
    $stmt->execute();
    $count = $stmt->rowCount();
    return $count == 0;
  }
  catch (Exception $e) {
    return false;
  }
}

?>