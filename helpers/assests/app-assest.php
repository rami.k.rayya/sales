<?php 

$cssFilesPaths = [
  "bootstrap" => "vendors/bootstrap-5.min.css",
  "main" => "layout/main.css",
  "authentication" => "actions/authentication.css",
  "fontAwesome" => "vendors/font-awesome-all.min.css",
  "person" => "actions/person.css",
];

$jsFilesPaths = [
  "jquery" => "vendors/jquery.min.js",
  "bootstrap" => "vendors/bootstrap.js",
  "person" => "actions/person.js",
  "confirmButton" => "components/confirmButton.js",
  "orderCalculations" => "components/orderCalculations.js"
];

/**
 * 
 * Load css files with paths
 * 
 * @param string $project_relative_root_path - The relative path of the root folder (project folder) from where the function
 * @param array $filesNames - Names of the files you want to include
 * 
 * @return string HTML that loads the files you provided
 * 
 */
function loadCss($project_relative_root_path, $filesNames) {
  global $cssFilesPaths;
  $cssLoadingTags = "";
  // Loop over files names array
  foreach ($filesNames as $fileName) {
    // generate link:css tag for each file name, and get the path from the css files paths array
    $cssLoadingTags .= "<link rel='stylesheet' href='" . $project_relative_root_path . "public/css/" . $cssFilesPaths[$fileName] . "'>";
  }
  return $cssLoadingTags;
}

/**
 * 
 * Load JS files with paths
 * 
 * @param string $project_relative_root_path - The relative path of the root folder (project folder) from where the function
 * @param array $filesNames - Names of the files you want to include
 * 
 * @return string JS files loading script tags <script src="filePath"></script>
 * 
 */
function loadJs($project_relative_root_path, $filesNames) {
  global $jsFilesPaths;
  $jsLoadingTags = "";
  // Loop over files names array
  foreach ($filesNames as $fileName)
    // Generate script:src tag for each file name, and get the file path from the hs files paths array
    $jsLoadingTags .= "<script src='" . $project_relative_root_path . "public/js/" . $jsFilesPaths[$fileName] . "'></script>";
  return $jsLoadingTags;
}

?>