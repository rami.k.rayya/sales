<?php

if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "db/connect-to-db.php";
include_once HELPERS_PATH . "db/query.php";

include_once CONTROLLERS_PATH . "authenticationController.php";

loginFirst();

if (isset($_GET["regId"])) {
  try {
    // Delete the category from the categories table
    delete("region", [
      "region.region_id = '" . $_GET["regId"] . "'"
    ]);

    // Redirect to category index
    header("Location: " . APPLICATION_ROOT_URL . "actions/region/region-index.php");
    exit();
  }
  catch (Exception $e) {
    echo "You can't delete this region, it is used elsewhere in customer or supervisor";
  }
}

?>