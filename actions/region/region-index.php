<?php

ob_start();
if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";

include_once CONTROLLERS_PATH . "/authenticationController.php";
include_once CONTROLLERS_PATH . "/regionController.php";

loginFirst();

$project_relative_root_path = "../../";

$regions = selectAll([], "region");

$usedRegions = getUsedRegions();

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Regions</title>
  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "fontAwesome", "main"]); ?>
</head>
<body>
  <!-- Include the navbar -->
  <?php include_once WIDGETS_PATH . "/navbar.php"; ?>

  <div class="content-wrapper">
    <div class="container">
      <table class="table table-striped table-hover table-bordered caption-top">
        <caption>
          <span class="badge bg-warning text-dark">
            <i class="fas fa-address-card"></i> List All Regions
          </span>

          <a href="<?= APPLICATION_ROOT_URL . "actions/region/create.php"; ?>" class="badge bg-dark index-operation-btn">
            <i class="fas fa-plus"></i> Create
          </a>
        </caption>
        <thead class="text-center">
          <th>#</th>
          <th><i class="far fa-user"></i> Name</th>
          <th><i class="fas fa-sliders-h"></i></th>
        </thead>
        <tbody class="text-center">
          <?php $regionsCount = count($regions); ?>        
          <?php if ($regionsCount == 0): ?>
            <tr>
              <td colspan="3">No Results</td>
            </tr>
          <?php endif; ?>
          <?php foreach ($regions as $index => $region): ?>
            <tr>
              <td><?= intval($index) + 1; ?></td>
              <td><?= $region["name"]; ?></td>
              <td>
                <a href="<?= APPLICATION_ROOT_URL . "actions/region/update.php?regId=" . $region["region_id"]; ?>" class="badge rounded-pill bg-info index-operation-btn">Update</a>

                <?php if (!$usedRegions || !in_array($region["region_id"], $usedRegions)): // The user can delete this region if it is not used elsewhere (not in this array or the array is null (it is false)) ?>
                  <a href="<?= APPLICATION_ROOT_URL . "actions/region/delete.php?regId=" . $region["region_id"]; ?>" class="badge rounded-pill bg-danger index-operation-btn confirm-btn">Delete</a>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
  <?= loadJs($project_relative_root_path, ["jquery", "confirmButton"]); ?>
</body>
</html>

<?php

ob_end_flush();

?>