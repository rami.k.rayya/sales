<?php

ob_start();
if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";
include_once HELPERS_PATH . "/db/query.php";

include_once CONTROLLERS_PATH . "/authenticationController.php";

loginFirst();

$project_relative_root_path = "../../";

$savingFailed = false;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (trim($_POST["name"]) == "")
    $savingFailed = true;
  else {
    insert("region", [
      "name" => $_POST["name"]
    ]);
    
    header("Location: " . APPLICATION_ROOT_URL . "actions/region/region-index.php");
    exit;
  }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Create New Region</title>
  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "fontAwesome", "main"]); ?>
</head>
<body>
  <!-- Include the navbar -->
  <?php include_once WIDGETS_PATH . "/navbar.php"; ?>
  
  <div class="content-wrapper" id="create-region-page">
    <form action="" id="region-create-form" method="post" autocomplete="off">
      <div class="container h-100">
        <h5>
          <span class="badge bg-info page-header-badge">Create new region</span>
        </h5>
        
        <div class="row">
          <div class="col-md-3">
            <label class="form-label" for="region-name">Name</label>
            <input type="text" class="form-control" name="name" id="region-name" placeholder="Region name" required>
          </div>

          <div class="form-group submit-btn-container">
            <button type="submit" id="create-btn" class="btn btn-dark">Create</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</body>
</html>

<?php

ob_end_flush();

?>