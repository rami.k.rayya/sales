<?php

ob_start();
if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";
include_once HELPERS_PATH . "/db/query.php";

include_once CONTROLLERS_PATH . "/authenticationController.php";
include_once CONTROLLERS_PATH . "/productController.php";

loginFirst();

$project_relative_root_path = "../../";

$savingFailed = false;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $savingFailed = !addProduct();
  if (!$savingFailed) {
    header("Location: " . APPLICATION_ROOT_URL . "actions/product/product-index.php");
    exit;
  }
}

$categories = selectAll([], "category");

if (!$categories) {
  echo "There is no created categories, please create new one to add a new product <br>";
  return;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Create New Product</title>
  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "fontAwesome", "main"]); ?>
</head>
<body>
  <!-- Include the navbar -->
  <?php include_once WIDGETS_PATH . "/navbar.php"; ?>
  
  <div class="content-wrapper" id="create-product-page">
    <form action="" id="product-create-form" method="post" autocomplete="off">
      <div class="container h-100">
        <h5>
          <span class="badge bg-info page-header-badge">Create new product</span>
        </h5>
        
        <div class="row">
          <div class="col-md-3">
            <label class="form-label" for="product-name">Name</label>
            <input type="text" class="form-control" name="name" id="product-name" placeholder="Product Name" required>
          </div>

          <div class="col-md-3">
            <label class="form-label" for="product-name">Quantity</label>
            <input type="number" class="form-control" name="available_qty" id="product-qty" placeholder="Product Quantity" required>
          </div>

          <div class="col-md-3">
            <label class="form-label" for="product-name">Price</label>
            <input type="number" class="form-control" name="price" id="product-price" placeholder="Product Price" required>
          </div>

          <div class="col-md-3">
            <label class="form-label" for="product-type">Category</label>
            <select name="cat_id" class="form-control" id="product-category" required>
              <?php foreach($categories as $category): ?>              
                <option value="<?= $category["cat_id"]; ?>"><?= $category["name"]; ?></option>
              <?php endforeach; ?>
            </select>
          </div>

          <div class="form-group submit-btn-container">
            <button type="submit" id="create-btn" class="btn btn-dark">Create</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</body>
</html>

<?php

ob_end_flush();

?>