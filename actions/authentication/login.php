<?php

ob_start();
session_start();

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";
include_once CONTROLLERS_PATH . "/authenticationController.php";

$project_relative_root_path = "../../";

if (!isGuest()) {
  // The user isn't guest so redirect him/her to the supervisors page
  header("Location: " . LOGIN_REDIRECTION_URL);
  die();
}

$loginFailed = false;

/**
 * If the Request is a post request so the user tried to login, so check his/her credentials
 * Check if the user email and password are in one record (same one) in the database
 */ 
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $userData = null;

  if (isset($_POST["user-email"]) && isset($_POST["user-password"])) {
    $userData = validateUser($_POST["user-email"], $_POST["user-password"]);
    $loginFailed = !$userData;
  }
  else $loginFailed = true;
  
  if (!$loginFailed) {
    // login succeeded
    // Save user data in the session

    $_SESSION["user"] = $userData["name"];
    $_SESSION["user_id"] = $userData["user_id"];
    
    header("Location: " . LOGIN_REDIRECTION_URL);

    die();
  }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login</title>

  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "main", "authentication"]); ?>
</head>
<body>

  <div class="content-wrapper" id="login-page">
    <form action="" class="h-100" id="login-form" method="post">
      <div class="container h-100">
        <div class="row h-100">

            <div class="col-4"></div>

            <div class="col-4">
              <div class="login-page-header">
                <span class="app-name">Sales</span> <span class="page-name">Login</span> 
              </div>

              <div class="login-card">
                <div class="mb-3">
                  <label for="user-email" class="form-label">Email address</label>
                  <input type="email" name="user-email" class="form-control <?= ($loginFailed) ? "is-invalid" : ""; ?>" id="user-email" placeholder="name@example.com" required>
                </div>

                <div class="mb-3">
                  <label for="user-password" class="form-label">Password</label>
                  <input type="password" name="user-password" class="form-control <?= ($loginFailed) ? "is-invalid" : ""; ?>" id="user-password" placeholder="Your password" required>
                </div>
                
                <div class="form-group">
                  <button type="submit" id="login-btn" class="btn btn-dark">Login</button>
                </div>

                <div class="form-group" id="login-message-container">
                  <?php if ($loginFailed): ?>
                    <div class="alert alert-danger" role="alert">
                      User email or passwrd is incorrect
                    </div>
                  <?php else: ?>
                    Sign In To Start Your Session
                  <?php endif; ?>
                </div>

              </div>

            </div>
            
            <div class="col-4"></div>
        </div>
      </div>
    </form>
  </div>

  <!-- Include JS files -->
  <?= loadJs($project_relative_root_path, ["jquery"]); ?>
</body>
</html>

<?php 

ob_end_flush();

?>