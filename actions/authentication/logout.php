<?php

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

session_start(); // Start The Session

session_unset(); // Unset The Data

session_destroy(); // Destory The Session

header("Location:" . LOGOUT_REDIRECTION_URL);

die();