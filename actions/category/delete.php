<?php

if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "db/connect-to-db.php";
include_once HELPERS_PATH . "db/query.php";

include_once CONTROLLERS_PATH . "authenticationController.php";

loginFirst();

if (isset($_GET["catId"])) {
  // Delete the category from the categories table
  delete("category", [
    "category.cat_id = '" . $_GET["catId"] . "'"
  ]);

  // Redirect to category index
  header("Location: " . APPLICATION_ROOT_URL . "actions/category/category-index.php");
  exit();
}

?>