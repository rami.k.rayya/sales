<?php

ob_start();
if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";

include_once HELPERS_PATH . "/db/query.php";

include_once CONTROLLERS_PATH . "/authenticationController.php";

loginFirst();

$project_relative_root_path = "../../";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (isset($_POST["catId"]) && isset($_POST["name"])) {
    update("category", [
      "category.cat_id = '" . $_POST["catId"] . "'"
    ], [
      "name" => $_POST["name"]
    ]);

    header("Location: " . APPLICATION_ROOT_URL . "actions/category/category-index.php");
    exit;
  }
  else {
    echo "Error occurred while updating the category <br>";
    exit;
  }
}

if (!isset($_GET["catId"])) {
  echo "Can't get the category you want, missing get parameter <br>";
  exit;
}

$category = selectOne([], "category", [
  "category.cat_id = '" . $_GET["catId"] . "'"
]);

if (!$category) {
  echo "Category doesn't exist <br>";
  exit;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Update Category</title>
  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "fontAwesome", "main"]); ?>
</head>
<body>
  <!-- Include the navbar -->
  <?php include_once WIDGETS_PATH . "/navbar.php"; ?>
  
  <div class="content-wrapper" id="update-person-page">
    <form action="" id="category-update-form" method="post" autocomplete="off">
      <div class="container h-100">
        <h5>
          <span class="badge bg-info page-header-badge">Update category</span>
        </h5>
        
        <div class="row">
          <input type="hidden" name="catId" value="<?= $category["cat_id"]; ?>">
          <div class="col-md-3">
            <label class="form-label" for="category-name">Name</label>
            <input type="text" class="form-control" name="name" id="category-name" placeholder="Category name" required value="<?= $category["name"]; ?>">
          </div>

          <div class="form-group submit-btn-container">
            <button type="submit" id="update-btn" class="btn btn-dark">update</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</body>
</html>

<?php

ob_end_flush();

?>