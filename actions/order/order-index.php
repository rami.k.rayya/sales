<?php

ob_start();
if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";

include_once CONTROLLERS_PATH . "authenticationController.php";
include_once CONTROLLERS_PATH . "orderController.php";

loginFirst();

$project_relative_root_path = "../../";

$orders = getOrders();

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Orders</title>
  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "fontAwesome", "main"]); ?>
</head>
<body>
  <!-- Include the navbar -->
  <?php include_once WIDGETS_PATH . "/navbar.php"; ?>

  <div class="content-wrapper">
    <div class="container">
      <table class="table table-striped table-hover table-bordered caption-top">
        <caption>
          <span class="badge bg-warning text-dark">
            <i class="fas fa-address-card"></i> List All Orders
          </span>

          <a href="<?= APPLICATION_ROOT_URL . "actions/order/order-report.php"; ?>" class="badge bg-dark index-operation-btn">
            <i class="far fa-clipboard"></i> &nbsp; Orders Reports
          </a>
        </caption>

        <thead class="text-center">
          <th>#</th>
          <th><i class="far fa-address-card"></i> Id</th>
          <th><i class="far fa-money-bill-alt"></i> Total price</th>
          <th>% Discount</th>
          <th><i class="far fa-money-bill-alt"></i> Total after discount</th>
          <th><i class="far fa-user"></i> Customer</th>
          <th><i class="far fa-user"></i> Created by</th>
          <th><i class="fas fa-sliders-h"></i></th>
        </thead>

        <tbody class="text-center">
          <?php $ordersCount = count($orders); ?>

          <?php if ($ordersCount == 0): ?>
            <tr>
              <td colspan="8">No Results</td>
            </tr>
          <?php endif; ?>
          
          <?php foreach ($orders as $index => $order): ?>
            <tr>
              <td><?= intval($index) + 1; ?></td>
              <td><?= $order["order_id"]; ?></td>
              <td><?= $order["total"]; ?></td>
              <td><?= $order["discount"]; ?></td>
              <td><?= $order["total_after_dis"]; ?></td>
              <td><?= $order["customer_name"]; ?></td>
              <td><?= $order["username"]; ?></td>
              <td>
                <a href="<?= APPLICATION_ROOT_URL . "actions/order/update.php?orderId=" . $order["order_id"]; ?>" class="badge rounded-pill bg-info index-operation-btn">Update</a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
  <?= loadJs($project_relative_root_path, ["jquery", "confirmButton"]); ?>
</body>
</html>

<?php

ob_end_flush();

?>