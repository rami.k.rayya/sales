<?php

ob_start();
if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";

include_once HELPERS_PATH . "/db/query.php";
include_once HELPERS_PATH . "/db/connect-to-db.php";

include_once CONTROLLERS_PATH . "/authenticationController.php";
include_once CONTROLLERS_PATH . "/orderController.php";

loginFirst();

$project_relative_root_path = "../../";

$orders = null;

$isRequestPost = $_SERVER['REQUEST_METHOD'] === 'POST';

if ($isRequestPost) {
  $customerId = null;
  $supervisor = null;
  $date = null;

  if (isset($_POST["customer_id"]) && trim($_POST["customer_id"]) != "") $customerId = $_POST["customer_id"];
  if (isset($_POST["supervisor"]) && trim($_POST["supervisor"]) != "") $supervisor = $_POST["supervisor"];
  if (isset($_POST["date"]) && trim($_POST["date"]) != "") $date = $_POST["date"];

  $orders = getOrders($customerId, $supervisor, $date);
}

$customers = selectAll([], "person", [
  "person.person_type = 3" 
]);

$supervisors = selectAll([], "person", [
  "person.person_type= '2'"
]);

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Order Update</title>
  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "fontAwesome", "main"]); ?>
</head>
<body>
  <!-- Include the navbar -->
  <?php include_once WIDGETS_PATH . "/navbar.php"; ?>
  
  <div class="content-wrapper" id="create-order-page">
    <form action="" id="order-update-form" method="post" autocomplete="off">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <label class="form-label" for="report-created-by">Customer</label>
            <select  class="form-control" name="customer_id" id="report-created-by">
              <option value=""></option>
              <?php foreach($customers as $i => $customer): ?>
                <option value="<?= $customer["person_id"]; ?>" <?= ($isRequestPost && $customer["person_id"] == $_POST["customer_id"]) ? "selected" : ""; ?> ><?= $customer["name"]; ?></option>
              <?php endforeach; ?>
            </select>
          </div>

          <div class="col-md-3">
            <label class="form-label" for="report-supervisor">Created by</label>
            <select class="form-control" name="supervisor" id="report-supervisor">
              <option value=""></option>
              <?php foreach($supervisors as $i => $supvervisor): ?>
                <option value="<?= $supvervisor["person_id"]; ?>" <?= ($isRequestPost && $supvervisor["person_id"] == $_POST["supervisor"]) ? "selected" : ""; ?> ><?= $supvervisor["name"]; ?></option>
              <?php endforeach; ?>
            </select>
          </div>

          <div class="col-md-3">
            <label class="form-label" for="report-supervisor">Date</label>
            <input class="form-control" type="date" name="date" id="report-date" value="<?= ($isRequestPost) ? $_POST["date"] : ""; ?>">
          </div>

          <div class="form-group submit-btn-container">
            <button type="submit" id="create-btn" class="btn btn-dark">Generate Report</button>
            <hr>
          </div>
        </div>
      </div>
    </form>

    
    <div class="container">
      <table class="table table-striped table-hover table-bordered caption-top">
        <thead class="text-center">
          <th>#</th>
          <th><i class="far fa-address-card"></i> Id</th>
          <th><i class="far fa-money-bill-alt"></i> Total price</th>
          <th>% Discount</th>
          <th><i class="far fa-money-bill-alt"></i> Total after discount</th>
          <th><i class="far fa-user"></i> Customer</th>
          <th><i class="far fa-user"></i> Created by</th>
          <th><i class="fas fa-sliders-h"></i></th>
        </thead>

        <tbody class="text-center">
          <?php if (!is_null($orders)): ?>
            <?php $ordersCount = count($orders); ?>

            <?php if ($ordersCount == 0): ?>
              <tr>
                <td colspan="8">No Results</td>
              </tr>
            <?php endif; ?>
            
            <?php foreach ($orders as $index => $order): ?>
              <tr>
                <td><?= intval($index) + 1; ?></td>
                <td><?= $order["order_id"]; ?></td>
                <td><?= $order["total"]; ?></td>
                <td><?= $order["discount"]; ?></td>
                <td><?= $order["total_after_dis"]; ?></td>
                <td><?= $order["customer_name"]; ?></td>
                <td><?= $order["username"]; ?></td>
                <td>
                  <a href="<?= APPLICATION_ROOT_URL . "actions/order/update.php?orderId=" . $order["order_id"]; ?>" class="badge rounded-pill bg-info index-operation-btn">Update</a>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php else: ?>
              <tr>
                <td colspan="8">No Results</td>
              </tr>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>

</body>
</html>

<?php 

ob_end_flush();

?>

