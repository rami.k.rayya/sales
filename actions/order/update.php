<?php

ob_start();
if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";

include_once HELPERS_PATH . "/db/query.php";
include_once HELPERS_PATH . "/db/connect-to-db.php";

include_once CONTROLLERS_PATH . "/authenticationController.php";
include_once CONTROLLERS_PATH . "/orderController.php";

loginFirst();

$project_relative_root_path = "../../";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  updateMaster();
  updateDetails();
  header("Location: " . APPLICATION_ROOT_URL . "actions/order/order-index.php");
  die();
}

if (!isset($_GET["orderId"])) {
  echo "Can't update this order, missing get parameters <br>";
  return;
}

$orderId = $_GET["orderId"];

$order = getOrder($orderId);

if (!$order) {
  echo "The order doesn't exist ;( <br>";
  return;
}

$orderDetails = getOrderDetails($orderId);

$products = selectAll([], "product", [
  "product.available_qty != 0"
]);

if (!$products) {
  echo "There is no products, please add at least one product <br>";
  return;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Order Update</title>
  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "fontAwesome", "main"]); ?>
</head>
<body>
  <!-- Include the navbar -->
  <?php include_once WIDGETS_PATH . "/navbar.php"; ?>
  
  <div class="content-wrapper" id="create-order-page">
    <form action="" id="order-update-form" method="post" autocomplete="off">
      <div class="container h-100">
        <h5>
          <span class="badge bg-info page-header-badge">Update Order</span>  
        </h5>

        <div class="row">
          <input type="hidden" name="order_id" value="<?= $order["order_id"]; ?>">
          <div class="col-md-3">
            <label class="form-label" for="order-customer">Customer</label>
            <input type="text" class="form-control" id="order-customer" disabled value="<?= $order["customer_name"]; ?>">
          </div>
          
          <div class="col-md-3">
            <label class="form-label" for="order-total">Total Price</label>
            <input type="text" class="form-control" name="total" id="order-total" readonly value="<?= $order["total"]; ?>">
          </div>

          <div class="col-md-2">
            <label class="form-label" for="order-discount">Discount</label>
            <input type="text" class="form-control" name="discount" id="order-discount" value="<?= $order["discount"]; ?>">
          </div>

          <div class="col-md-2">
            <label class="form-label" for="order-after-discount">Total Net Price</label>
            <input type="text" class="form-control" name="total_after_discount" id="order-after-discount" readonly value="<?= $order["total_after_dis"]; ?>">
          </div>

          <div class="col-md-2">
            <label class="form-label" for="order-after-discount">Created by</label>
            <input type="text" class="form-control" id="order-created-by" disabled value="<?= $order["username"]; ?>">
          </div>
        </div>

        <hr>

        <h5>
          <span class="badge bg-dark page-header-badge">Details : </span>  
        </h5>

        <div class="row">
          <div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col" class="text-center">#</th>
                  <th scope="col">Product Name</th>
                  <th scope="col">Quantity</th>
                  <th scope="col" class="text-center">Available Quantity</th>
                </tr>
              </thead>
              <tbody>
                <?php if ($orderDetails): ?>
                  <?php foreach ($orderDetails as $index => $detail): ?>
                    <tr>
                      <td class="text-center"><b><?= intval($index) + 1; ?></b></td>
                      <td>
                        <select class="form-control product-details-input" name="details[<?= $index; ?>][product_id]">
                          <?php foreach ($products as $i => $product): ?>
                            <option value="<?= $product["product_id"]; ?>" <?= ($product["product_id"] == $detail["product_id"]) ? "selected" : ""; ?> product-price="<?= $product["price"]; ?>"><?= $product["name"]; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </td>
                      <td>
                        <input type="hidden" value="<?= $detail["pro_ord_id"]; ?>" name="details[<?= $index; ?>][pro_ord_id]">
                        <input class="form-control qty-details-input" type="number" value="<?= $detail["quantity"]; ?>" name="details[<?= $index; ?>][quantity]" required min="0" max="<?= $detail["available_qty"]; ?>">
                      </td>
                      <td class="text-center"><b><?= $detail["available_qty"]; ?><b></td>
                    </tr>
                  <?php endforeach; ?>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="form-group submit-btn-container">
            <button type="submit" id="create-btn" class="btn btn-dark">Update</button>
          </div>
        </div>
      </div>
    </form>
  </div>
  <?= loadJs($project_relative_root_path, ["jquery", "orderCalculations"]); ?>
</body>
</html>

<?php 

ob_end_flush();

?>

