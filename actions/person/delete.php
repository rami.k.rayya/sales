<?php

if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "db/connect-to-db.php";
include_once HELPERS_PATH . "db/query.php";

include_once CONTROLLERS_PATH . "authenticationController.php";
include_once CONTROLLERS_PATH . "personController.php";


loginFirst();

if (isset($_GET["userId"])) { // Delete supervisor or admin
  
  $givenId = intval(trim($_GET["userId"])); // Get the id of the user

  // Get the data of the user
  $userData = selectOne(["person_id"], "user", [
    "user.user_id = '" . $givenId . "'"
  ]);
  
  if ($userData) { // if the user exists
    // Get the data of the person
    $personData = selectOne(["person_type"], "person", [
      "person.person_id = '" . $userData["person_id"] . "'"
    ]);

    if ($personData["person_type"] == "1") { // The user is admin
      $admins = getAllAdmins();
      if (count($admins) == 1) { // If there is only one admin, he/she shouldn't be deleted
        header("Location: " . APPLICATION_ROOT_URL . "actions/person/admins-index.php");
        exit;
      }
    }

    // Delete the user
    delete("user", [  
      "user.user_id = '" . $givenId . "'"
    ]);

    // Delete the person
    delete("person", [
      "person.person_id = '" . $userData["person_id"] . "'"
    ]);

    if ($_SESSION["user_id"] == $givenId) // The user deleted his/her own account so we should redirect him/her to the logout page
      header("Location: " . APPLICATION_ROOT_URL . "actions/authentication/logout.php");
    else {
      if ($personData["person_type"] == "1") // The deleted user is admin so redirect the user into admins index
        header("Location: " . APPLICATION_ROOT_URL . "actions/person/admins-index.php");
      else // The deleted user is supervisor so redirect the user into supervisors index
        header("Location: " . APPLICATION_ROOT_URL . "actions/person/supervisors-index.php");
    }

    exit;
  }
  else echo "The user isn't exist.";
}
else { // Delete a customer
  $givenId = intval(trim($_GET["personId"]));

  // Delete the customer from the person table
  delete("person", [
    "person.person_id = '" . $givenId . "'"
  ]);

  // Redirect the user who delete the customer to the customers index
  header("Location: " . APPLICATION_ROOT_URL . "actions/person/customers-index.php");
  exit();
}

?>