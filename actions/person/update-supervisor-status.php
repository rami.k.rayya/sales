<?php

if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "db/connect-to-db.php";
include_once HELPERS_PATH . "db/query.php";

include_once CONTROLLERS_PATH . "authenticationController.php";
include_once CONTROLLERS_PATH . "personController.php";

loginFirst();

if (isset($_GET["userId"]) && isset($_GET["newStatus"])) {
  if ($_GET["newStatus"] == "1" || $_GET["newStatus"] == "0") {
      if (changeSupervisorStatus($_GET["userId"], $_GET["newStatus"])) {
        header("Location: " . APPLICATION_ROOT_URL . "actions/person/supervisors-index.php");
      }
      else
        echo "Something went wrong, can't change user status.<br>";
  }
  else 
    echo "Something went wrong, can't change user status.<br>";
}
else
  echo "Something went wrong, can't change user status.<br>";
