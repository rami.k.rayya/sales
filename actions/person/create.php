<?php

ob_start();
if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";
include_once HELPERS_PATH . "/db/query.php";

include_once CONTROLLERS_PATH . "/authenticationController.php";
include_once CONTROLLERS_PATH . "/personController.php";

loginFirst();

$project_relative_root_path = "../../";

// Get all regions in the database
$regions = selectAll(["region_id", "name"], "region");

$isEmailUniqueOrEmpty = true;
$savingFailed = false;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

  if (trim($_POST["email"]) != "") $isEmailUniqueOrEmpty = isUnique("email", "user", $_POST["email"]);

  if ($isEmailUniqueOrEmpty) {
    $savingFailed = !personCreate();

    if (!$savingFailed) { // Saving Succeed
      $redirectionUrl = "";
      
      // Redirection url depend on the person type if it is admin so we need to redirect him to the admins index and same for supervisors and customers
      if ($_POST["person-type"] == '1') $redirectionUrl = "admins-index";
      else if ($_POST["person-type"] == '2') $redirectionUrl = "supervisors-index";
      else if ($_POST["person-type"] == '3') $redirectionUrl = "customers-index";

      header("Location: " . APPLICATION_ROOT_URL . "actions/person/" . $redirectionUrl . ".php");
      die();
    }
  }
  else $savingFailed = true;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Create New Person</title>
  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "fontAwesome", "main", "person"]); ?>
</head>
<body>
  <!-- Include the navbar -->
  <?php include_once WIDGETS_PATH . "/navbar.php"; ?>
  
  <div class="content-wrapper" id="create-person-page">
    <form action="" id="person-create-form" method="post" autocomplete="off">
      <div class="container h-100">
        <h5>
          <span class="badge bg-info page-header-badge">Create new person</span>  
        </h5>
        
        <div class="row">
          <div class="col-md-2">
            <label class="form-label" for="person-type">Person Type</label>
            <select name="person-type" class="form-control" id="person-type" required>
              <option value=""></option>
              <option value="1" data-show-user-fields="true">Admin</option>
              <option value="2" data-show-user-fields="true">Supervisor</option>
              <option value="3" data-show-user-fields="false">Customer</option>
            </select>
          </div>

          <div class="col-md-3">
            <label class="form-label" for="person-name">Name</label>
            <input type="text" class="form-control" name="name" id="person-name" placeholder="Username" required>
          </div>

          <div class="col-md-3">
            <label class="form-label" for="person-mobile">Mobile</label>
            <input type="number" class="form-control" name="mobile" id="person-mobile" placeholder="User Mobile" required>
          </div>

          <div class="col-md-2">
            <label class="form-label" for="person-address">Address</label>
            <input type="text" class="form-control" name="address" id="person-address" placeholder="User Address" required>
          </div>

          <div class="col-md-2">
            <label class="form-label" for="person-region">Region</label>
            <select type="text" class="form-control" name="region" id="person-region" placeholder="User Region">
              <option value="">No Region</option>
              <?php foreach($regions as $index => $region): ?>
                <option value="<?= $region["region_id"]; ?>">
                  <?= $region["name"]; ?>
                </option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>

        <?php 
          // We should hide user fields if the person-type set and its value is 3 (customer) or if the person type isn't set (which is at the first time the user enter the page)
          $hideUserFields = (isset($_POST["person-type"]) && $_POST["person-type"] == "3") || !isset($_POST["person-type"]); 
        ?>

        <div class="row">
          <div class="col-md-3 <?= ($hideUserFields) ? "d-none" : ""; ?> user-field-container">
            <label class="form-label" for="person-address">Email</label>
            <input <?= ($hideUserFields) ? "readonly" : ""; ?> type="email" class="form-control <?= (!$isEmailUniqueOrEmpty) ? "is-invalid" : ""; ?>" name="email" id="user-email" placeholder="User email" autocomplete="on">
            <div class="invalid-feedback">
                Email must be unique, This email is taken
            </div>
          </div>

          <div class="col-md-3 <?= ($hideUserFields) ? "d-none" : ""; ?> user-field-container">
            <label class="form-label" for="person-address">Password</label>
            <input <?= ($hideUserFields) ? "readonly" : ""; ?> type="password" class="form-control" name="password" id="user-password" placeholder="User password" autocomplete="on">
          </div>

          <div class="col-md-3 <?= ($hideUserFields) ? "d-none" : ""; ?> user-field-container">
            <label class="form-label" for="person-address">Rewrite The Password</label>
            <input <?= ($hideUserFields) ? "readonly" : ""; ?> type="password" class="form-control" name="password-check" id="user-password-check" placeholder="Rewrite the previous password" autocomplete="on">
            <div class="invalid-feedback">
              Passwords are different
            </div>
            <div class="valid-feedback">
              Looks good, passwords are identical
            </div>
          </div>
        </div>

        <div class="row">
          <div class="form-group submit-btn-container">
            <button type="submit" id="create-btn" class="btn btn-dark">Create</button>
          </div>
        </div>
      </div>
    </form>
  </div>

  <!-- Include JS files -->
  <?= loadJs($project_relative_root_path, ["jquery", "person"]); ?>
</body>
</html>

<?php 

ob_end_flush();

?>