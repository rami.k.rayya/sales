<?php

ob_start();
if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";

include_once CONTROLLERS_PATH . "/authenticationController.php";
include_once CONTROLLERS_PATH . "/personController.php";

loginFirst();

$project_relative_root_path = "../../";

$currentPageName = "supervisors";

$supervisors = getAllSupervisors();

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Supervisors</title>
  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "fontAwesome", "main"]); ?>
</head>
<body>
  <!-- Include the navbar -->
  <?php include_once WIDGETS_PATH . "/navbar.php"; ?>

  <div class="content-wrapper">
    <div class="container">
      <table class="table table-striped table-hover table-bordered caption-top">
        <caption>
          <span class="badge bg-warning text-dark">
            <i class="fas fa-address-card"></i> List All Supervisors
          </span>

          <a href="<?= APPLICATION_ROOT_URL . "actions/person/create.php"; ?>" class="badge bg-dark index-operation-btn">
            <i class="fas fa-plus"></i> Create
          </a>
        </caption>
        <thead class="text-center">
          <th>#</th>
          <th><i class="fas fa-at"></i> Email</th>
          <th><i class="far fa-user"></i> Name</th>
          <th><i class="fas fa-phone"></i> Mobile</th>
          <th><i class="fas fa-map-marker-alt"></i> Address</th>
          <th><i class="fas fa-sliders-h"></i></th>
        </thead>
        <tbody class="text-center">
          <?php if (count($supervisors) == 0): ?>
            <tr>
              <td colspan="6">No Results</td>
            </tr>
          <?php endif; ?>

          <?php foreach ($supervisors as $index => $supervisor): ?>
            <tr class="<?= ($supervisor["user_status"] == "1") ? "table-info" : "table-danger"; ?>">
              <td><?= intval($index) + 1; ?></td>
              <td><?= $supervisor["email"]; ?></td>
              <td><?= $supervisor["name"]; ?></td>
              <td><?= $supervisor["mobile"]; ?></td>
              <td><?= $supervisor["address"]; ?></td>
              <td>
                <a href="<?= APPLICATION_ROOT_URL . "actions/person/update.php?userId=" . $supervisor["user_id"]; ?>" class="badge rounded-pill bg-info index-operation-btn">Update</a>
                <a href="<?= APPLICATION_ROOT_URL . "actions/person/delete.php?userId=" . $supervisor["user_id"]; ?>" class="badge rounded-pill bg-danger index-operation-btn confirm-btn">Delete</a>
                <?php if ($supervisor["user_status"] == "0"): // Disabled ?>
                  <a href="<?= APPLICATION_ROOT_URL . "actions/person/update-supervisor-status.php?userId=" . $supervisor["user_id"] . "&newStatus=1"; ?>" class="badge rounded-pill bg-primary index-operation-btn">Activate</a>
                <?php else: ?>
                  <a href="<?= APPLICATION_ROOT_URL . "actions/person/update-supervisor-status.php?userId=" . $supervisor["user_id"] . "&newStatus=0"; ?>" class="badge rounded-pill bg-dark index-operation-btn">Disable</a>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
  <?= loadJs($project_relative_root_path, ["jquery", "confirmButton"]); ?>
</body>
</html>

<?php 

ob_end_flush();

?>