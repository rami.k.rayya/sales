<?php

ob_start();
if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";

include_once HELPERS_PATH . "/db/query.php";
include_once HELPERS_PATH . "/db/connect-to-db.php";

include_once CONTROLLERS_PATH . "/authenticationController.php";
include_once CONTROLLERS_PATH . "/personController.php";

loginFirst();

$project_relative_root_path = "../../";

$userId = isset($_GET["userId"]) ? $_GET["userId"] : false;
$personId = isset($_GET["personId"]) ? $_GET["personId"] : false;

$userData = null;

if ($userId) { // The user is an admin or a supervisor
  // Select the user from the two tables user and person
  $queryStr = "SELECT 
              user.*,
              person.*
  FROM `user`
  INNER JOIN `person` ON user.person_id = person.person_id
  WHERE user.user_id = '" . $userId . "'";

  $stmt = $connection->prepare($queryStr);
  $stmt->execute();
  $userData = $stmt->fetch();
}
else if ($personId && !$userId) { // The user is a customer
  $userData = selectOne([], "person", [
    "person.person_id = '" . $personId . "'"
  ]);
}
else {
  // Error occurred get params are not provided
  echo "Not enough get parameters provided";
  return;
}

if (!$userData) { // The user doesn't exist
  echo "Error occurred while fetching your data or the user doesn't exist";
  return;
}
else { // The user
  if ($userData["person_type"] == "1") { // The user is admin
    // The admin user can't be updated from another admin
    if ($_SESSION["user_id"] != $userId) {
      echo "You can't edit this account";
      return;
    }
  }
}

// Get all regions in the database
$regions = selectAll(["region_id", "name"], "region");

$savingFailed = false;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $savingFailed = !personUpdate();

  $redirectionUrl = "";

  // Redirection url depend on the person type if it is admin so we need to redirect him to the admins index and same for supervisors and customers
  if ($userData["person_type"] == '1') $redirectionUrl = "admins-index";
  else if ($userData["person_type"] == '2') $redirectionUrl = "supervisors-index";
  else if ($userData["person_type"] == '3') $redirectionUrl = "customers-index";
  header("Location: " . APPLICATION_ROOT_URL . "actions/person/" . $redirectionUrl . ".php");

  die();

  return;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Person Update</title>
  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "fontAwesome", "main", "person"]); ?>
</head>
<body>
  <!-- Include the navbar -->
  <?php include_once WIDGETS_PATH . "/navbar.php"; ?>
  
  <div class="content-wrapper" id="create-person-page">
    <form action="" id="person-update-form" method="post" autocomplete="off">
      <input type="hidden" name="user-id" value="<?= $userData["user_id"]; ?>">
      <input type="hidden" name="person-id" value="<?= $userData["person_id"]; ?>">
      <div class="container h-100">
        <h5>
          <span class="badge bg-info page-header-badge">update</span>  
        </h5>

        <div class="row">
          <div class="col-md-2">
            <label class="form-label" for="person-type">Person Type</label>

            <?php
              $personType = "";
              if ($userData["person_type"] == "1") $personType = "Admin";
              else if ($userData["person_type"] == "2") $personType = "Supervisor";
              else if ($userData["person_type"] == "3") $personType = "Customer";
            ?>
            <input type="text" class="form-control" disabled value="<?= $personType; ?>">
          </div>

          <div class="col-md-3">
            <label class="form-label" for="person-name">Name</label>
            <input type="text" class="form-control" name="name" id="person-name" value="<?= $userData["name"]; ?> " placeholder="Username" required>
          </div>

          <div class="col-md-3">
            <label class="form-label" for="person-mobile">Mobile</label>
            <input type="number" class="form-control" name="mobile" id="person-mobile" value="<?= $userData["mobile"]; ?>" placeholder="User Mobile" required>
          </div>

          <div class="col-md-2">
            <label class="form-label" for="person-address">Address</label>
            <input type="text" class="form-control" name="address" id="person-address" value="<?= $userData["address"]; ?>" placeholder="User Address" required>
          </div>

          <div class="col-md-2">
            <label class="form-label" for="person-region">Region</label>
            <select type="text" class="form-control" name="region" id="person-region" placeholder="User Region" <?= (!$userId) ? "required" : ""; ?>>
              <?php if ($userId): ?>
                <option value="">No Region</option>
              <?php endif; ?>
              <?php foreach($regions as $index => $region): ?>
                <option value="<?= $region["region_id"]; ?>" <?= ($userData["reg_id"] == $region["region_id"]) ? "selected" : ""; ?> >
                  <?= $region["name"]; ?>
                </option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>

        <div class="row">
          <?php if ($userId != false): // Show these inputs, if the user is not false ?>
            <div class="col-md-3 user-field-container">
              <label class="form-label" for="person-address">Email</label>
              <input type="text" value="<?= ($userId) ? $userData["email"] : ""; ?>" class="form-control" name="email" placeholder="User email" autocomplete="on" disabled>
              <div class="invalid-feedback">
                  Email must be unique, This email is taken
              </div>
            </div>

            <div class="col-md-3 user-field-container">
              <label class="form-label" for="person-address">Password</label>
              <input type="password" class="form-control" name="password" id="user-password" placeholder="New password" autocomplete="on">
            </div>

            <div class="col-md-3 user-field-container">
              <label class="form-label" for="person-address">Rewrite The Password</label>
              <input value="" type="password" class="form-control" name="password-check" id="user-password-check" placeholder="Rewrite the previous password" autocomplete="on">
              <div class="invalid-feedback">
                Passwords are different
              </div>
              <div class="valid-feedback">
                Looks good, passwords are identical
              </div>
            </div>
          <?php endif; ?>
        </div>

        <div class="row">
          <div class="form-group submit-btn-container">
            <button type="submit" id="create-btn" class="btn btn-dark">Update</button>
          </div>
        </div>
      </div>
    </form>
  </div>

  <!-- Include JS files -->
  <?= loadJs($project_relative_root_path, ["jquery", "person"]); ?>
</body>
</html>

<?php 

ob_end_flush();

?>