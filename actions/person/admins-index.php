<?php

ob_start();
if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";

include_once CONTROLLERS_PATH . "/authenticationController.php";
include_once CONTROLLERS_PATH . "/personController.php";

loginFirst();

$project_relative_root_path = "../../";
$currentPageName = "admins";

$admins = getAllAdmins();

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Admins</title>
  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "fontAwesome", "main"]); ?>
</head>
<body>
  <!-- Include the navbar -->
  <?php include_once WIDGETS_PATH . "/navbar.php"; ?>

  <div class="content-wrapper">
    <div class="container">
      <table class="table table-striped table-hover table-bordered caption-top">
        <caption>
          <span class="badge bg-warning text-dark">
            <i class="fas fa-address-card"></i> List All Admins
          </span>

          <a href="<?= APPLICATION_ROOT_URL . "actions/person/create.php"; ?>" class="badge bg-dark index-operation-btn">
            <i class="fas fa-plus"></i> Create
          </a>
        </caption>
        <thead class="text-center">
          <th>#</th>
          <th><i class="fas fa-at"></i> Email</th>
          <th><i class="far fa-user"></i> Name</th>
          <th><i class="fas fa-phone"></i> Mobile</th>
          <th><i class="fas fa-map-marker-alt"></i> Address</th>
          <th><i class="fas fa-sliders-h"></i></th>
        </thead>
        <tbody class="text-center">
          <?php $adminsCount = count($admins); ?>        
          <?php if ($adminsCount == 0): ?>
            <tr>
              <td colspan="6">No Results</td>
            </tr>
          <?php endif; ?>
          <?php foreach ($admins as $index => $admin): ?>
            <tr class="<?= $admin["user_id"] == $_SESSION["user_id"] ? "table-warning" : "";?>">
              <td><?= intval($index) + 1; ?></td>
              <td><?= $admin["email"]; ?></td>
              <td><?= $admin["name"]; ?></td>
              <td><?= $admin["mobile"]; ?></td>
              <td><?= $admin["address"]; ?></td>
              <td>
                <?php if ($_SESSION["user_id"] == $admin["user_id"]): // The user (admin) can only update his/her account only ?>
                  <a href="<?= APPLICATION_ROOT_URL . "actions/person/update.php?userId=" . $admin["user_id"]; ?>" class="badge rounded-pill bg-info index-operation-btn">Update</a>
                  
                  <?php if ($adminsCount != 1) : // The last can't be deleted ?>
                    <a href="<?= APPLICATION_ROOT_URL . "actions/person/delete.php?userId=" . $admin["user_id"]; ?>" class="badge rounded-pill bg-danger index-operation-btn confirm-btn" confirm-message="You will be logged out and your account will be deleted permanently.">Delete</a>
                  <?php endif; ?>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
  <?= loadJs($project_relative_root_path, ["jquery", "confirmButton"]); ?>
</body>
</html>

<?php 

ob_end_flush();

?>