<?php

ob_start();
if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "/assests/app-assest.php";

include_once CONTROLLERS_PATH . "/authenticationController.php";
include_once CONTROLLERS_PATH . "/personController.php";

loginFirst();

$project_relative_root_path = "../../";
$currentPageName = "customers";

$customers = getAllCustomers();

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Customers</title>
  <!-- Include CSS files -->
  <?= loadCss($project_relative_root_path, ["bootstrap", "fontAwesome", "main"]); ?>
</head>
<body>
  <!-- Include the navbar -->
  <?php include_once WIDGETS_PATH . "/navbar.php"; ?>

  <div class="content-wrapper">
    <div class="container">
      <table class="table table-striped table-hover table-bordered caption-top">
        <caption>
          <span class="badge bg-warning text-dark">
            <i class="fas fa-address-card"></i> List All Customers
          </span>

          <a href="<?= APPLICATION_ROOT_URL . "actions/person/create.php"; ?>" class="badge bg-dark index-operation-btn">
            <i class="fas fa-plus"></i> Create
          </a>
        </caption>
        <thead class="text-center">
          <th>#</th>
          <th><i class="fas fa-at"></i> Email</th>
          <th><i class="fas fa-phone"></i> Mobile</th>
          <th><i class="fas fa-map-marker-alt"></i> Address</th>
          <th><i class="fas fa-dolly-flatbed"></i> Region</th>
          <th><i class="fas fa-sliders-h"></i></th>
        </thead>
        <tbody class="text-center">
          <?php if (count($customers) == 0): ?>
            <tr>
              <td colspan="7">No Results</td>
            </tr>
          <?php endif; ?>
          <?php foreach ($customers as $index => $customer): ?>
            <tr>
              <td><?= intval($index) + 1; ?></td>
              <td><?= $customer["name"]; ?></td>
              <td><?= $customer["mobile"]; ?></td>
              <td><?= $customer["address"]; ?></td>
              <td><?= $customer["region_name"]; ?></td>
              <td>
                <a href="<?= APPLICATION_ROOT_URL . "actions/person/update.php?personId=" . $customer["person_id"]; ?>" class="badge rounded-pill bg-info index-operation-btn">Update</a>
                <a href="<?= APPLICATION_ROOT_URL . "actions/person/delete.php?personId=" . $customer["person_id"]; ?>" class="badge rounded-pill bg-danger index-operation-btn confirm-btn">Delete</a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
  <?= loadJs($project_relative_root_path, ["jquery", "confirmButton"]); ?>
</body>
</html>

<?php 

ob_end_flush();

?>