<?php 

/**
 * These routes only work for php include but not for CSS or JS
 * Because it is the full path of the folder not a relative path
 */
define("PROJECT_ROOT_PATH", $_SERVER['DOCUMENT_ROOT'] . "/sales/");
define("ACTIONS_PATH", PROJECT_ROOT_PATH . "actions/");
define("CONTROLLERS_PATH", PROJECT_ROOT_PATH . "controllers/");
define("HELPERS_PATH", PROJECT_ROOT_PATH . "helpers/");
define("WIDGETS_PATH", PROJECT_ROOT_PATH . "widgets/");

?>