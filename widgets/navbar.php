<?php

if (session_status() === PHP_SESSION_NONE) session_start(); // Start session if it was not started

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";

include_once CONTROLLERS_PATH . "authenticationController.php";

$project_relative_root_path = "../../";
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container">
    <a class="navbar-brand" href="">Sales</a>
    <div class="navbar-collapse">
      <div class="navbar-nav w-100">
        <?php if (isGuest()): ?>
          <a class="nav-link ms-auto" href="<?= APPLICATION_ROOT_URL . "actions/authentication/login.php"; ?>">
            Login
          </a>
        <?php else: ?>
            <a class="nav-link <?= (isset($currentPageName) && $currentPageName == 'admins') ? "active" : ""; ?>" href="<?= APPLICATION_ROOT_URL . "actions/person/admins-index.php"; ?>">Admins</a>
            <a class="nav-link <?= (isset($currentPageName) && $currentPageName == 'supervisors') ? "active" : ""; ?>" href="<?= APPLICATION_ROOT_URL . "actions/person/supervisors-index.php"; ?>">Supervisors</a>
            <a class="nav-link <?= (isset($currentPageName) && $currentPageName == 'customers') ? "active" : ""; ?>" href="<?= APPLICATION_ROOT_URL . "actions/person/customers-index.php"; ?>">Customers</a>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbar-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Others
              </a>
              <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbar-dropdown">
                <li><a class="dropdown-item" href="<?= APPLICATION_ROOT_URL . "actions/order/order-index.php"; ?>">Orders</a></li>
                <li><a class="dropdown-item" href="<?= APPLICATION_ROOT_URL . "actions/category/category-index.php"; ?>">Categories</a></li>
                <li><a class="dropdown-item" href="<?= APPLICATION_ROOT_URL . "actions/region/region-index.php"; ?>">Regions</a></li>
                <li><a class="dropdown-item" href="<?= APPLICATION_ROOT_URL . "actions/product/product-index.php"; ?>">Products</a></li>
              </ul>
            </li>

            <a class="nav-link ms-auto <?= (isset($currentPageName) && $currentPageName == 'profile') ? "active" : ""; ?>" href="<?= APPLICATION_ROOT_URL . "actions/person/update.php?userId=" . $_SESSION["user_id"]; ?>">
              <i class="far fa-user"></i>
              <?= $_SESSION["user"]; ?>
            </a>
            <a class="nav-link" href="<?= APPLICATION_ROOT_URL . "actions/authentication/logout.php"; ?>">
              <i class="fas fa-sign-out-alt"></i> 
              Logout
            </a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</nav>

<?= loadJs($project_relative_root_path, ["bootstrap"]); ?>