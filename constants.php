<?php 


define("HOST_NAME", "localhost");
define("APPLICATION_ROOT_URL", "http://" . HOST_NAME . "/sales/");
define("LOGIN_REDIRECTION_URL", APPLICATION_ROOT_URL . "actions/person/supervisors-index.php");
define("LOGOUT_REDIRECTION_URL", APPLICATION_ROOT_URL . "actions/authentication/login.php");


?>