<?php

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";
include_once HELPERS_PATH . "db/connect-to-db.php";
include_once HELPERS_PATH . "db/query.php";
include_once CONTROLLERS_PATH . "authenticationController.php";

// Login Actions

/**
 * 
 * Check if the user exists by checking the given credentials
 * 
 * @param string $email The email of the user account
 * @param string $password The password of the user account
 * 
 * @return array|false Array Contains the user data | False the user isn't exist
 * 
 */
function validateUser($email, $password) {
  $hashedPassword = hash("sha256", $password);

  global $connection;

  // Select the user from the two table user and person
  $queryStr = "SELECT user.user_id, person.name FROM `user` INNER JOIN `person` ON user.person_id = person.person_id
                WHERE user.email = '{$email}' 
                AND user.password = '{$hashedPassword}'
                AND person.person_type = '1'";

  $stmt = $connection->prepare($queryStr);
  $stmt->execute();
  $userData = $stmt->fetch();

  return $userData;
}

/**
 * Check if the user is a guest (Not logged in)
 * 
 * @return boolean True if the user is guest False if the user logged in
 */
function isGuest() {
  return !isset($_SESSION["user"]);
}

/**
 * If the user is a guest redirect him to login page to login
 * 
 * @return void
 */
function loginFirst() {
  if (isGuest()) {
    header("Location: " . APPLICATION_ROOT_URL . "actions/authentication/login.php");
    die();
  }
}
?>