<?php

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "db/connect-to-db.php";
include_once HELPERS_PATH . "db/query.php";

/**
 * Add a new record to the person table
 * 
 * @return false|int False failed | Int the id of the inserted row
 */
function addPerson() {
  if (
    isset($_POST["name"]) &&
    isset($_POST["mobile"]) &&
    isset($_POST["address"]) &&
    isset($_POST["person-type"]) &&
    isset($_POST["region"])
  ) {
    $columnsAndValues = [
      "name" => $_POST["name"],
      "mobile" => $_POST["mobile"],
      "address" => $_POST["address"],
      "person_type" => $_POST["person-type"],
    ];

    // Check if the region is not null so add it to the columns and values array 
    if (trim($_POST["region"]) != "") // Not Null
      $columnsAndValues["reg_id"] = $_POST["region"];

    return insert("person", $columnsAndValues, true);
  }
  else return false;
}

/**
 * Add a new user to the user table
 * 
 * @param int $person_id
 * 
 * @return boolean True succeed | False Failed
 */
function addUser($person_id) {
  if (
    isset($_POST["email"]) &&
    isset($_POST["password"]) &&
    isset($_POST["password-check"])
  ) {
    if ($_POST["password"] !== $_POST["password-check"]) return false;
    return insert("user", [
      "email" => $_POST["email"],
      "password" => hash('sha256', $_POST["password"]),
      "person_id" => $person_id
    ]);
  }
  else return false;
}

/**
 * 
 * Control the process of creating new person (Any type of person)
 * 
 * @return boolean True succeed | False Failed
 */
function personCreate() {
  if (isset($_POST["person-type"])) {
    if ($_POST["person-type"] == '1' || $_POST["person-type"] == '2') { 
      // Admin or Supervisor, so we need to call add user function after adding the person
      $person_id = addPerson();
      if ($person_id) // If the person id is an int (any int is true except zero but zero is not an index value)
        return addUser($person_id);
      else return false;
    }
    return addPerson();
  }
}

/**
 * 
 * Update a specific person record in person table
 * 
 * @return int 0 indicates no records were updated | 1 record was updated
 */
function updatePerson() {
   if (
    isset($_POST["name"]) &&
    isset($_POST["mobile"]) &&
    isset($_POST["address"])
  ) {

    $columnsAndValues = [
      "name" => $_POST["name"],
      "mobile" => $_POST["mobile"],
      "address" => $_POST["address"],
    ];

    // Check if the region is not null so add it to the columns and values array 
    if (trim($_POST["region"]) != "") // Not Null
      $columnsAndValues["reg_id"] = $_POST["region"];

    return
    update("person", [
      "person.person_id = '" . $_POST["person-id"] . "'"
    ], $columnsAndValues);
  }
}

/**
 * 
 * Update specific user record in the database
 * 
 * @return int 0 indicates no records were updated | 1 record was updated
 */
function updateUser() {
  if (
    isset($_POST["password"]) &&
    isset($_POST["password-check"])
  ) {
    if (trim($_POST["password"]) == "" && trim($_POST["password-check"]) == "") return true;
    if ($_POST["password"] != $_POST["password-check"]) return false;

    $columnsAndValues = [
      "password" => hash('sha256', $_POST["password"]),
    ];

    return 
    update("user", [
      "user.user_id = '" . $_POST["user-id"] . "'"
    ], $columnsAndValues);
  }
}

/**
 * 
 * Control the process of person update
 * 
 * @return boolean True update succeed | False update failed
 * 
 */
function personUpdate() {
  if (isset($_POST["user-id"])) {
    return updatePerson() && updateUser();
  }
  else if (isset($_POST["person-id"]))
    return updatePerson();
}

/**
 * 
 * Get all the admins records from the database
 * 
 * @return array|false Array of records | False failed or there is no data
 * 
 */
function getAllAdmins() {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  // Select the user from the two tables user and person
  $queryStr = "SELECT 
              user.user_id,
              user.email,
              person.name,
              person.mobile,
              person.address
  FROM `user`
  INNER JOIN `person` ON user.person_id = person.person_id
  WHERE person.person_type = '1'";

  $stmt = $connection->prepare($queryStr);
  $stmt->execute();
  $users = $stmt->fetchAll();

  return $users;
}

/**
 * 
 * Get all the supervisors records from the database
 * 
 * @return array|false Array of records | False failed or there is no data
 * 
 */
function getAllSupervisors() {
// NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  // Select the user from the two tables user and person
  $queryStr = "SELECT 
              user.user_id,
              user.email,
              user.user_status,
              person.name,
              person.mobile,
              person.address
  FROM `user`
  INNER JOIN `person` ON user.person_id = person.person_id
  WHERE person.person_type = '2'";

  $stmt = $connection->prepare($queryStr);
  $stmt->execute();
  $users = $stmt->fetchAll();

  return $users;
}

/**
 * 
 * Get all the customers records from the database
 * 
 * @return array|false Array of records | False failed or there is no data
 * 
 */
function getAllCustomers() {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  // Select the user from the two tables user and person
  $queryStr = "SELECT 
              person.person_id,
              person.name,
              person.mobile,
              person.address,
              region.name AS region_name
  FROM `person`
  INNER JOIN `region` ON person.reg_id = region.region_id
  WHERE person.person_type = '3'";

  $stmt = $connection->prepare($queryStr);
  $stmt->execute();
  $users = $stmt->fetchAll();

  return $users;
}

/**
 * 
 * Get the type of the user (admin (1) or supervisor (3))
 * 
 * @param int|string $userId The id of the user (admin or supervisor)
 * 
 * @return string|false The type of the person or false if the user doesn't exist
 * 
 */
function getUserType($userId) {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  // Select the user from the two tables user and person
  $queryStr = "SELECT 
              person.person_type
  FROM `user`
  INNER JOIN `person` ON user.person_id = person.person_id
  WHERE user.user_id = '${userId}'";

  $stmt = $connection->prepare($queryStr);
  $stmt->execute();
  $user = $stmt->fetch();

  if ($user)
    return $user["person_type"];
  else
    return false;
}

/**
 * 
 * Change the supervisor status
 * 
 * @param int|string $userId The is of the supervisor
 * @param int|string $status The new status of the supervisor
 * 
 * @return int 0 no records were updated | 1 one record was updated
 * 
 */
function changeSupervisorStatus($userId, $status) {
  if ($status != "1" && $status != "0") return false;

  // Check if the user is supervisor or admin (only supervisors can be disabled or activated)
  if (getUserType($userId) == "1") return false;

  return update("user", [
    "user.user_id = '" . $userId . "'",
  ], [
    "user_status" => $status
  ]);
}

?>
