<?php

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "db/connect-to-db.php";
include_once HELPERS_PATH . "db/query.php";

/**
 * Get all orders from orders table
 * 
 * @param string|int $customerId (optional) Customer id filter
 * @param string|int $supervisorId (optional) Supervisor id filter
 * @param string $date (optional) Date order filter
 * 
 * @return array|false Array contains all the orders from the table | False => fetchaing failed or there is no orders
 */
function getOrders($customerId = null, $supervisorId = null, $date = null) {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  $queryStr = "SELECT orders.*, customer.name AS customer_name, supervisor.name AS username
  FROM orders
  INNER JOIN person AS supervisor ON orders.created_by = supervisor.person_id
  INNER JOIN person AS customer ON orders.customer_id = customer.person_id ";
  
  $whereStatementsArr = [];

  if (!is_null($customerId)) $whereStatementsArr[] = "orders.customer_id = '${customerId}'";
  if (!is_null($supervisorId)) $whereStatementsArr[] = "orders.created_by = '${supervisorId}'";
  if (!is_null($date)) $whereStatementsArr[] = "orders.order_created_date = '${date}'";

  if (count($whereStatementsArr) > 0)
    $queryStr .= whereQueryStr($whereStatementsArr);

  $query = $connection->prepare($queryStr);
  $query->execute();

  return $query->fetchAll();
}

/**
 * 
 * Get the order data from the database
 * 
 * @param int|string
 * 
 * @return array|false Array contains the columns and the values of those columns | False The record doesn't exist
 * 
 */
function getOrder($orderId) {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  $queryStr = "SELECT orders.*, customer.name AS customer_name, supervisor.name AS username
  FROM orders
  INNER JOIN person AS supervisor ON orders.created_by = supervisor.person_id
  INNER JOIN person AS customer ON orders.customer_id = customer.person_id
  WHERE orders.order_id = ${orderId}";

  $query = $connection->prepare($queryStr);
  $query->execute();

  return $query->fetch();
}

/**
 * Get the order details
 * @param string|int The id of the order (The master part)
 * 
 * @return array|false Array of the order details | False => There is no details
 */
function getOrderDetails($orderId) {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  $queryStr = "SELECT pro_ord.*, product.name AS product_name, product.available_qty
  FROM pro_ord
  INNER JOIN product ON pro_ord.product_id = product.product_id
  WHERE pro_ord.order_id = '${orderId}'";

  $query = $connection->prepare($queryStr);
  $query->execute();

  return $query->fetchAll();
}

/**
 * Update the master part of the order
 * 
 * @return boolean True => Succeeded | False => Failed
 */
function updateMaster() {
  if (
    isset($_POST["order_id"]) &&
    isset($_POST["total"]) &&
    isset($_POST["discount"]) &&
    isset($_POST["total_after_discount"])
  ) {
    return update("orders", [
      "orders.order_id = '" . $_POST["order_id"] . "'"
    ], [
      "total" => $_POST["total"],
      "discount" => $_POST["discount"],
      "total_after_dis" => $_POST["total_after_discount"]
    ]);
  }
  else return false;
}

/**
 * Update the details of the order
 * 
 * @return boolean True => Succeeded | False => Failed
 */
function updateDetails() {
  if (isset($_POST["details"])) {
    foreach ($_POST["details"] as $i => $detail) {
      update("pro_ord", [
        "pro_ord.pro_ord_id = '" . $detail["pro_ord_id"] . "'"
      ], [
        "quantity" => $_POST["details"][$i]["quantity"],
        "product_id" => $_POST["details"][$i]["product_id"]
      ]);
    }
    return true;
  }
  else return false;
}