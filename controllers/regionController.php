<?php

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "db/connect-to-db.php";
include_once HELPERS_PATH . "db/query.php";

/**
 * Get the used regions in person table
 * This function may help to get all the regions ids in the person table
 * to know wether the region is used or not so you can delete it or not (without foreign key problems)
 * 
 * @return array|false Array of the used ids | false no results
 * 
 */
function getUsedRegions() {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  $queryStr = selectQueryStr(["reg_id"], "person") . "WHERE person.reg_id IS NOT NULL" . " GROUP BY person.reg_id";

  $query = $connection->prepare($queryStr);
  $query->execute();
  $idsArrays = $query->fetchAll();

  if (!$idsArrays) return false;
  
  $arrayOfIds = [];
  foreach ($idsArrays as $idArray) $arrayOfIds[] = $idArray["reg_id"]; // Populate all ids into array of ids array

  return $arrayOfIds;
}