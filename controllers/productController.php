<?php

include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/paths.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/sales/constants.php";

include_once HELPERS_PATH . "db/connect-to-db.php";
include_once HELPERS_PATH . "db/query.php";

/**
 * 
 * Get all the products
 * 
 * @return array|false Array contains all the records | false there is no records
 *
 */
function getProducts() {
  // NOTE : The global variables stored in array called $GLOBALS
  if (!array_key_exists('connection', $GLOBALS)) include "connect-to-db.php"; // Include DB connection file if the global connection variable isn't provided

  global $connection;

  $queryStr =
  "SELECT product.*, category.name AS category_name
   FROM product
   INNER JOIN category ON product.cat_id = category.cat_id";
  
  $query = $connection->prepare($queryStr);
  $query->execute();

  return $query->fetchAll();
}

/**
 * Insert a product into product table
 * 
 * @return boolean True => Succeeded | False => Failed
 */
function addProduct() {
  if (
    isset($_POST["name"]) &&
    isset($_POST["available_qty"]) &&
    isset($_POST["price"]) &&
    isset($_POST["cat_id"])
  ) {
    return insert("product", [
      "name" => $_POST["name"],
      "available_qty" => $_POST["available_qty"],
      "price" => $_POST["price"],
      "cat_id" => $_POST["cat_id"],
    ]);
  }
  else return false;
}

/**
 * 
 * Update specific product in product table
 * 
 * @return boolean True => succeeded | False => Failed
 * 
 */
function updateProduct() {
  if (
    isset($_POST["productId"]) &&
    isset($_POST["name"]) &&
    isset($_POST["available_qty"]) &&
    isset($_POST["price"]) &&
    isset($_POST["cat_id"])
  ) {
    return update("product", [
      "product.product_id = '" . $_POST["productId"] . "'"
    ], [
      "name" => $_POST["name"],
      "available_qty" => $_POST["available_qty"],
      "price" => $_POST["price"],
      "cat_id" => $_POST["cat_id"],
    ]);
  }
  else return false;
}